<?php
//TERMINAL BEKASI

namespace App\Http\Controllers\cctv;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;

class terminalbekasi extends Controller
{
    public function shows_cctv_terminal_bekasi()
    {
        $id = 40;
        //GET DATA
        $url   = DB::table('cctv')->where('id', $id)->value('link_server');
        $loop  = DB::table('cctv')->where('id', $id)->value('loop');
        //GET
        $save  = DB::table('cctv')->where('id', $id)->value('link');
        //NOW
        if($url != "maintenance"){
            $allFiles = Storage::files('');
                //ITERASI 8
                if ($loop == 1) {
                    $name = "bekasi" . "1" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 2, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi5/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 2) {
                    $name = "bekasi" . "2" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 3, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi6/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 3) {
                    $name = "bekasi" . "3" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 4, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi7/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 4) {
                    $name = "bekasi" . "4" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 5, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi8/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 5) {
                    $name = "bekasi" . "5" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 6, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi1/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 6) {
                    $name = "bekasi" . "6" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 7, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi2/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 7) {
                    $name = "bekasi" . "7" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 8, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi3/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 8) {
                    $name = "bekasi" . "8" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 1, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^bekasi4/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } else {
                    DB::table('cctv')
                        ->where('id', $id)
                        ->update(['loop' => 1]);
                }
                //GET IMG
            $link  = DB::table('cctv')->where('id', $id)->value('link');
            $onair = DB::table('cctv')->where('id', $id)->value('save');
            }else{
                $onair = "maintenance.jpg";
                $save = "maintenance.jpg";
                $link = "maintenance.jpg";
            }
            return view('front-panel-rttmc.cctv.cctv-show.terminal-bekasi', compact('link', 'save','onair'));
    }
}