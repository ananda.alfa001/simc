<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Storage;
use Carbon\Carbon;

class GuestController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function login()
    {
        return view('auth.login');
    }
    public function register()
    {
        return view('auth.register');
    }
    ////ACTION FRONT-PANEL-RTTMC
    public function cctv_show()
    {
        // $url = "http://36.67.208.195/ciasem/axis-cgi/jpg/image.cgi?resolution=6$idx480&.jpg";
        // $name = "ciasem".".jpg";
        // Storage::put($name, file_get_contents($url));

        return view('front-panel-rttmc.cctv.cctv-panel');
    }


    
}
