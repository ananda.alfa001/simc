<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use Carbon\Carbon;
use Alert;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    protected $redirectTo = RouteServiceProvider::HOME;
    
    public function __construct()
    {
        $this->middleware('peternakan');
    
    }

    //REGISTER USER PETERNAKAN
    protected function view_store_1()
    {
        return view('auth.register-roles.register-peternakan');
    }
    protected function view_store_2()
    {
        $farm = db::table('peternakan')
        ->where('id_user',Auth::user()->id)
        ->value('jumlah_kandang');
        // ->get();
        return view('auth.register-roles.kandang.register-kandang-peternakan', compact('farm'));
    }

    protected function store(request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required', 'string', 'max:255',
            'email' => 'required', 'string', 'max:255', 'unique:users',
            'roles' => 'required',
            'password' => 'required','string',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $validate = $request->validate([
                        'name' => ['required'],
                        'email' => ['required'],
                        'roles' => ['required'],
                        'password' => ['required'],
                    ]);
                    $validate['password'] = Hash::make($validate['password']);
                    $user = User::create($validate);
                    Auth::login($user);
                    //input detil roles
                    $name = Auth::user()->name;
                    if(Auth::user()->roles == "peternakan" ){
                        Alert::Success('Success', $name.' Berhasil Terdaftar Sebagai Peternakan! Silahkan lengkapi detil form berikut.');
                        return view('auth.register-roles.register-peternakan');
                    }elseif($validate['roles'] == "kemitraan" ){
                        Alert::Success('Success', $name.' Berhasil Terdaftar Sebagai Kemitraan! Silahkan lengkapi detil form berikut.');
                        return view('auth.register-roles.register-kemitraan');
                    }elseif($validate['roles'] == "teknisi" ){
                        Alert::Success('Success', $name.' Berhasil Terdaftar Sebagai Teknisi! Silahkan lengkapi detil form berikut.');
                        return view('auth.register-roles.register-teknisi');
                    }else{
                        Auth::logout();
                        request()->session()->invalidate();
                        request()->session()->regenerateToken();
                        Alert::error('Error', 'Bagian Tidak Terdaftar, Anda Logout!');
                        return redirect()->back();
                    }
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }
    //------------REGISTER USER DETIL PETERNAKAN (form1-up)
    protected function store_1(request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required', 'string', 'max:255',
            'year' => 'required', 'date',
            'farm_owner' => 'required', 'string', 'max:255', 'unique:users',
            'cage' => 'required','number',
            'address' => 'required','string', 'max:255',
            'telephone_number' => 'required','number',
            'file' => 'required|mimes:doc,docx,pdf,jpg,jpeg,png|max:5048',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $id_peternakan = DB::table('peternakan')->insertGetId([
                                    'id_user' => Auth::user()->id,
                                    'nama_peternakan' => $request->name,
                                    'tahun_berdiri' => $request->year,
                                    'nama_pemilik' => $request->farm_owner,
                                    'alamat' => $request->address,
                                    'no_hp' => $request->telephone_number,
                                    'created_at' => Carbon::now()
                                ]);
                    $uploadedFile = $request->file('file');
                    $path = $uploadedFile->store('public/files/persetujuan/'.$id_peternakan);
                    foreach($request->cage as $key => $value)
                    {
                        db::table('kandang')->insert([
                            'id_peternakan' => $id_peternakan,
                            'nama_kandang' => $value
                        ]);
                    }
                    DB::table('peternakan')
                    ->where('id',$id_peternakan)
                    ->update([
                        'jumlah_kandang' => $request->cages,
                        'lembar_persetujuan' => $path,
                    ]);
                    Alert::success('Detil Farm Succes!');
                    return redirect('/peternakan');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }
    //------------REGISTER USER DETIL PETERNAKAN FORM-2
	public function Upload_KemitraanMicro(){
		return view('auth.register-roles.upload.upload-micro');
	}
	public function Upload_KemitraanMacro(){
		return view('auth.register-roles.upload.upload-macro');
	}
    //REGISTER USER TEKNISI

    //REGISTER USER KEMITRAAN
}    
