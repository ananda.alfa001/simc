<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ciasem extends Controller
{
//SHOW CCTV ALL
    public function shows_cctv_ciasem()
    {
        //CIASEM
        $id = 1;
        //GET DATA
        $data =  DB::table('cctv')->where('id', $id)
                ->get();
        foreach ($data as $key) {
            $loop  = $key->loop;
            $save  = $key->link;
            $onair = $key->save;
            $url   = $key->link_server;
        }
        if($url != "maintenance"){
            $allFiles = Storage::files('');
                //ITERASI 8
                if ($loop == 1) {
                    $name = "ciasem" . "1" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 2, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem5/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 2) {
                    $name = "ciasem" . "2" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 3, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem6/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 3) {
                    $name = "ciasem" . "3" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 4, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem7/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 4) {
                    $name = "ciasem" . "4" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 5, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem8/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 5) {
                    $name = "ciasem" . "5" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 6, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem1/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 6) {
                    $name = "ciasem" . "6" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 7, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem2/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 7) {
                    $name = "ciasem" . "7" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 8, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem3/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } elseif ($loop == 8) {
                    $name = "ciasem" . "8" . Carbon::Now()->timestamp . ".jpg";
                    Storage::put($name, file_get_contents($url));
                    DB::table('cctv')->where('id', $id)
                                     ->update(['loop' => 1, 'link' => $name, 'save' => $save]);
                    //DELETE
                    if ($matchingFiles = preg_grep("/^ciasem4/i", $allFiles)) {
                        Storage::delete($matchingFiles);
                    }
                } else {
                    DB::table('cctv')
                        ->where('id', $id)
                        ->update(['loop' => 1]);
                }
                foreach ($data as $key) {
                    $link  = $key->link;
                }
                return view('front-panel-rttmc.cctv.cctv-show.ciasem', compact('link', 'save','onair'));
        }else{
            $onair = "maintenance.jpg";
            $save = "maintenance.jpg";
            $link = "maintenance.jpg";
            return view('front-panel-rttmc.cctv.cctv-show.terminal-bekasi', compact('link', 'save','onair'));
        }
    }
}