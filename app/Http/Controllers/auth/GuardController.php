<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Alert;

class GuardController extends Controller
{
    //first login
    public function AuthRoles(request $request)
    {   
        if(Auth::check()){
            $name = Auth::user()->name;
            $roles = Auth::user()->roles;
            if($roles == "operator"){
                //login
            }else{
                Auth::logout();
                request()->session()->invalidate();
                request()->session()->regenerateToken();
                Alert::error('Login Gagal', 'Coba Kembali!');
                return redirect('login');
            }       
    }else{
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        Alert::error('Login Gagal', 'Coba Kembali!');
        return redirect('login');
    }
}

    public function AuthGuard(request $request)
    {   
        if(Auth::check()){
            $name = Auth::user()->name;
            if (Auth::user()->roles == 'operator') {
            //login
            }else{
                Auth::logout();
                request()->session()->invalidate();
                request()->session()->regenerateToken();
                Alert::error('Login Failed', 'Try Again!');
                return redirect('login');
            }
        }else{
            Alert::error('You are not logged in', 'Please Login First!');
            return redirect('login');
        }
    }
}
