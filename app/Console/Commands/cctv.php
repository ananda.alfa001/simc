<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class cctv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cctv:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take snapshot cctv to show image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return 0;
    }
}
