<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\auth\LoginController;
use App\Http\Controllers\auth\GuardController;
use App\Http\Controllers\auth\RegisterController;
use App\Http\Controllers\cctv\terminalbekasi;
use App\Http\Controllers\cctv\ciasem;
use App\Http\Controllers\cctv;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front-panel-rttmc.front-panel');
});

Route::get('/update',[cctv::class,'login'])->name('login');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login',[GuestController::class,'login'])->name('login');
    Route::get('/register',[GuestController::class,'register'])->name('register');
    Route::post('/log-up',[LoginController::class,'authenticate'])->name('authenticate');
    Route::post('/register-up',[RegisterController::class,'store'])->name('register-guest');
    //ACTION FRONT-PANEL-RTTMC
    Route::get('/cctv',[GuestController::class,'cctv_show'])->name('cctv-show');
});

Route::middleware(['auth'])->group(function () {
    //SESSION GUARD
    Route::get('/Guard',[GuardController::class,'AuthGuard'])->name('AuthGuard');
    Route::get('/Auth-Roles',[GuardController::class,'AuthRoles'])->name('AuthRoles');
    Route::get('/logout',[LoginController::class,'logout'])->name('dashboard-logout');  
});

//ROUTE CCTV
Route::group(['middleware' => 'guest'], function () {
     Route::get('/terminal-bekasi',[Terminalbekasi::class,'shows_cctv_terminal_bekasi']);
     Route::get('/ciasem',[ciasem::class,'shows_cctv_ciasem']);
});             
