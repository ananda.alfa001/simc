@extends('front-panel-rttmc.main.main')
@section('content')
  <!-- end slider -->
  <section class="intro">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 wow" data-splitting>
          <h3 class="section-title">ROAD TRANSPORT AND TRAFFIC<br>
            MANAGEMENT CENTER</h3>
        </div>
        <!-- end col-5 -->
        <div class="col-lg-7 wow" data-splitting>
          <p>sistem yang dapat digunakan mendukung untuk mempercepat
            proses dan mengkoordinasikan kegiatan secara teratur dan tersistematis sehingga
            implementasi dilapangan dapat terkontrol dan terkendali.</p>
          <h6>Direktorat Lalu Lintas Perhubungan Darat</h6>
          <small>Kementerian Perhubungan Republik Indonesia</small> <b>10</b>
          <h4>TAHUN<br> 
            KUALITAS - IKHLAS - NYAMAN - SELAMAT</h4>
        </div>
        <!-- end col-7 --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </section>

  <section class="intro-image">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="office-slider">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <figure class="reveal-effect masker wow"> <img src="asset/front-panel/images/office01.png" alt="Image">
                  <figcaption>
                    <h6> HEADQUARTOR OF TOUROG</h6>
                  </figcaption>
                </figure>
              </div>
              <!-- end swiper-slide -->
              <div class="swiper-slide">
                <figure> <img src="asset/front-panel/images/office01.png" alt="Image">
                  <figcaption>
                    <h6> TORONTO OFFICE</h6>
                  </figcaption>
                </figure>
              </div>
              <!-- end swiper-slide -->
              <div class="swiper-slide">
                <figure> <img src="asset/front-panel/images/office01.png" alt="Image">
                  <figcaption>
                    <h6> HEADQUARTOR OF TOUROG</h6>
                  </figcaption>
                </figure>
              </div>
              <!-- end swiper-slide --> 
            </div>
            <!-- end swiper-wrapper -->
            <div class="swiper-pagination"></div>
            <!-- end swiper-pagination --> 
          </div>
          <!-- end office-slider --> 
        </div>
        <!-- end col-12 --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </section>
  <!-- end intro-image -->
  <section class="icon-content-block">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 wow" data-splitting>
          <h3 class="section-title">RTTMC<br>
            Road Transport and Traffic Management Center</h3>
        </div>
        <!-- end col-12 -->
        <div class="col-lg-3 col-md-4 wow" data-splitting>
          <div class="content-block">
            <figure> <img src="asset/front-panel/images/hub.png" alt="Image"> </figure>
            <h6>Kementerian Perhubungan Republik Indonesia</h6>
            <ul>
              <li>.......</li>
              <li>.......</li>
              <li>.......</li>
              <li>............</li>
            </ul>
          </div>
          <!-- end content-block --> 
        </div>
        <!-- end col-3 -->
        <div class="col-lg-3 col-md-4 wow" data-splitting>
          <div class="content-block selected">
            <figure> <img src="asset/front-panel/images/hubdat.png" alt="Image"> </figure>
            <h6>Direktorat Jenderal Perhubungan Darat</h6>
            <ul>
              <li>.......</li>
              <li>.......</li>
              <li>.......</li>
              <li>............</li>
            </ul>
          </div>
          <!-- end content-block --> 
        </div>
        <!-- end col-3 -->
        <div class="col-lg-3 col-md-4 wow" data-splitting>
          <div class="content-block">
            <figure> <img src="asset/front-panel/images/hubdat.png" alt="Image"> </figure>
            <h6>Direktorat Lalu Lintas Jalan</h6>
            <ul>
              <li>.......</li>
              <li>.......</li>
              <li>.......</li>
              <li>............</li>
            </ul>
          </div>
          <!-- end content-block --> 
        </div>
        <!-- end col-3 --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </section>
  <!-- end icon-content-block -->
  <section class="works">
    <ul>
      <li>
        <figure class="reveal-effect masker wow"> <a href="asset/front-panel/images/works01.jpg" data-fancybox><img src="asset/front-panel/images/works01.jpg" alt="Image"></a> </figure>
        <div class="caption wow" data-splitting>
          <h3>NAMA</h3>
          <small>JABATAN</small> </div>
        <!-- end caption --> 
      </li>
      <li>
        <figure class="reveal-effect masker wow"> <a href="asset/front-panel/images/works02.jpg" data-fancybox><img src="asset/front-panel/images/works02.jpg" alt="Image"></a> </figure>
        <div class="caption wow" data-splitting>
          <h3>Goddes Cover Art</h3>
          <small>PRINT, DIGITAL, DEVELOPMENT</small></div>
        <!-- end caption --> 
      </li>
      <li>
        <figure class="reveal-effect masker wow"> <a href="asset/front-panel/images/works03.jpg" data-fancybox><img src="asset/front-panel/images/works03.jpg" alt="Image"></a> </figure>
        <div class="caption wow" data-splitting>
          <h3>Hard Employee</h3>
          <small>WEB, DIGITAL, DEVELOPMENT</small> </div>
        <!-- end caption --> 
      </li>
      <li>
        <figure class="reveal-effect masker wow"> <a href="asset/front-panel/images/works04.jpg" data-fancybox><img src="asset/front-panel/images/works04.jpg" alt="Image"></a> </figure>
        <div class="caption wow" data-splitting>
          <h3>Sweet Berry Pie</h3>
          <small>DIGITAL, PRINT, DEVELOPMENT</small> </div>
        <!-- end caption --> 
      </li>
      <li>
        <figure class="reveal-effect masker wow"> <a href="asset/front-panel/images/works05.jpg" data-fancybox><img src="asset/front-panel/images/works05.jpg" alt="Image"></a> </figure>
        <div class="caption wow" data-splitting>
          <h3>King of Roosters</h3>
          <small>PRINT, DIGITAL, DEVELOPMENT</small> </div>
        <!-- end caption --> 
      </li>
      <li>
        <figure class="reveal-effect masker wow"> <a href="asset/front-panel/images/works06.jpg" data-fancybox><img src="asset/front-panel/images/works06.jpg" alt="Image"> </a></figure>
        <div class="caption wow" data-splitting>
          <h3>Primero  Car</h3>
          <small>WEB, DIGITAL, DEVELOPMENT</small> </div>
        <!-- end caption --> 
      </li>
    </ul>
  </section>
  <!-- end works -->
  <section class="clients">
    <div class="container">
      <div class="row"> <div class="col-lg-5 wow" data-splitting>
          <h3 class="section-title">AGENSY PROUD<br>
            IS QUALITY OF<br>
            PARTNERS</h3>
        </div>
        <!-- end col-5 -->
        <div class="col-lg-7">
          <ul>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo01.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo02.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo03.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo04.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo05.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo06.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo07.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo08.png" alt="Image"> </li>
            <li class="reveal-effect masker wow"> <img src="asset/front-panel/images/logo09.png" alt="Image"> </li>
          </ul>
        </div>
        <!-- end col-7 --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </section>
  @endsection