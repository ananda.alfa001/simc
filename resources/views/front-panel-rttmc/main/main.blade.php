<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="format-detection" content="RTTMC">
<meta name="theme-color" content="#75dab4"/>
<title>RTTMC | Kementerian Perhubungan Republik Indonesia</title>
<meta name="IDNesia" content="RTTMC">
<meta name="description" content="RTTMC | Kementerian Perhubungan Republik Indonesia">
<meta name="keywords" content="RTTMC, rttmc, kemenhub, dephub, cctv, lalulintas, jalan, perjalanan, digital, kamera, mudik, pulkam, perjalanan, dishub">

<!-- SOCIAL MEDIA META -->
<meta property="og:description" content="RTTMC | Kementerian Perhubungan Republik Indonesia">
<meta property="og:site_name" content="RTTMC">
<meta property="og:title" content="RTTMC">
<meta property="og:type" content="website">
	@include('front-panel-rttmc.main.components.style')

</head>
<body>
	<!-- ======= Header ======= -->
	@include('front-panel-rttmc.main.components.navbar')
	<!-- ============== -->
	@include('front-panel-rttmc.main.components.aside-header')
	<!-- ============== -->
	 <main id="main" class="main">
		<section class="section dashboard">
			@yield('content')
		</section>
	 </main>
	<!-- ======= Footer ======= -->
	<footer id="footer" class="footer">
		@include('front-panel-rttmc.main.components.footer')
	</footer><!-- End Footer -->
	<!-- /page container -->
		@include('front-panel-rttmc.main.components.script')
</body>
</html>
