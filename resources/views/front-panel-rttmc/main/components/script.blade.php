<!-- Vendor JS Files -->
<script src="{{ asset('asset/front-panel/js/jquery.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/swiper.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/wow.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/splitting.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/odometer.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/fancybox.min.js') }}"></script> 
<script src="{{ asset('asset/front-panel/js/scripts.js') }}"></script>