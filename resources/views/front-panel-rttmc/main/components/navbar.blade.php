<div class="preloader">
  <div class="layer"></div>
  <!-- end layer -->
  <div class="inner">
    <figure> <img src="asset/front-panel/images/preloader.gif" alt="Image"> </figure>
    <span>RTTMC Loading</span> </div>
  <!-- end inner --> 
</div>
<!-- end preloader -->
<div class="page-transition">
  <div class="layer"></div>
  <!-- end layer --> 
</div>
<!-- end page-transition -->
<nav class="site-navigation">
  <div class="layer"></div>
  <!-- end layer -->
  <div class="inner">
    <ul data-splitting>
      <li><a href="/">CCTV</a><i class="fas fa-caret-down"></i> <small>First page</small>
      <ul>
      	<li><a href="index.html">JALUR UTARA</a></li>
      	<li><a href="index-video.html">JALUT TENGAH</a></li>
      	<li><a href="index-carousel.html">JALUR SELATAN</a></li>
      </ul>
        </li>
      <li><a href="studio.html">TRAFFIC COUNTING</a> <small>kepadatan lalu lintas</small> </li>
      <li><a href="showcases.html">NEWS RTTMC</a> <small>Our all projects</small> </li>
      <li><a href="blog.html">Si Dilan</a> <small>Recent posts</small> </li>
      <li><a href="contact.html">LOGIN</a> <small>khusus RTTMC Crew</small> </li>
    </ul>
  </div>
  <!-- end inner --> 
</nav>
<!-- end site-navigation -->
<div class="social-media">
  <div class="layer"> </div>
  <!-- end layer -->
  <div class="inner">
    <h5>Social Share </h5>
    <ul>
      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
      <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
      <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
      <li><a href="#"><i class="fab fa-youtube"></i></a></li>
    </ul>
  </div>
</div>
<!-- end social-media -->
<div class="all-cases">
  <div class="layer"> </div>
  <!-- end layer -->
  <div class="inner">
    <ul>
      <li><a href="#">NEWS</a></li>
      <li><a href="#">CCTV</a></li>
      <li><a href="#">TRAFFIC COUNTING</a></li>
      <li><a href="#">ATMS</a></li>
      <li><a href="#">SIDILAN</a></li>
      <li><a href="#">LOGIN</a></li>
    </ul>
  </div>
  <!-- end inner --> 
</div>
<!-- end all-cases -->