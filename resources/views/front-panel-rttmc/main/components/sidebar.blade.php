 <!-- ======= Sidebar ======= -->
 <aside id="sidebar" class="sidebar">
     <ul class="sidebar-nav" id="sidebar-nav">
         <li class="nav-item">
             <a class="nav-link collapsed" href="/peternakan">
                 <i class="bi bi-grid"></i>
                 <span>Dashboard</span>
             </a>
         </li><!-- End Dashboard Nav -->

         @if (Auth::user()->roles == 'admin')
             <li class="nav-item">
                 <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
                     <i class="bi bi-layout-text-window-reverse"></i><span>Data Admin</span><i
                         class="bi bi-chevron-down ms-auto"></i>
                 </a>
                 <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                     <li>
                         <a href="/peternakan">
                             <i class="bi bi-circle"></i><span>Daftar Peternakan</span>
                         </a>
                     </li>
                     <li>
                         <a href="/dashboard-admin/patienttable">
                             <i class="bi bi-circle"></i><span>Daftar Teknisi</span>
                         </a>
                     </li>
                     <li>
                         <a href="#">
                             <i class="bi bi-circle"></i><span>Daftar Kemitraan</span>
                         </a>
                     </li>
                 </ul>
             </li><!-- End Tables Nav -->
         @endif

         <li class="nav-heading">Account Profile</li>
         <li class="nav-item">
             <a class="nav-link collapsed" href="{{route('User-detils')}}">
                 <i class="bi bi-person"></i>
                 <span>Profile</span>
             </a>
         </li><!-- End Profile Page Nav -->
         <li class="nav-item">
             <a class="nav-link collapsed" href="pages-contact.html">
                 <i class="bi bi-envelope"></i>
                 <span>Contact</span>
             </a>
         </li><!-- End Contact Page Nav -->
     </ul>
 </aside><!-- End Sidebar-->
