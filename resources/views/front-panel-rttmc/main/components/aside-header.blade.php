<aside class="left-side">
    <div class="logo"> <a href="index.html"><img src="asset/front-panel/images/hub.png" alt="Image"></a> </div>
    <!-- end logo -->
    <div class="hamburger" id="hamburger">
      <div class="hamburger__line hamburger__line--01">
        <div class="hamburger__line-in hamburger__line-in--01"></div>
      </div>
      <div class="hamburger__line hamburger__line--02">
        <div class="hamburger__line-in hamburger__line-in--02"></div>
      </div>
      <div class="hamburger__line hamburger__line--03">
        <div class="hamburger__line-in hamburger__line-in--03"></div>
      </div>
      <div class="hamburger__line hamburger__line--cross01">
        <div class="hamburger__line-in hamburger__line-in--cross01"></div>
      </div>
      <div class="hamburger__line hamburger__line--cross02">
        <div class="hamburger__line-in hamburger__line-in--cross02"></div>
      </div>
    </div>
    <!-- end hamburger -->
    <div class="follow-us"> IKUTI KAMI </div>
    <!-- end follow-us -->
    <div class="equalizer"> <span></span> <span></span> <span></span> <span></span> </div>
    <!-- end equalizer --> 
  </aside>
  <!-- end left-side -->
  <div class="all-cases-link"> 
    <span>INFORMASI</span> 
    <b>+</b> 
  </div>
  <!-- end all-cases-link -->
  <header class="slider">
    <div class="swiper-container gallery-top">
      <div class="swiper-wrapper">
        <div class="swiper-slide" data-background="asset/front-panel/images/cctvall.jpg"></div>
        <div class="swiper-slide">
          <video src="asset/front-panel/videos/video01.2.mp4" muted autoplay loop></video>
        </div>
        <div class="swiper-slide" data-background="asset/front-panel/images/tc.jpg"></div>
      </div>
      <!-- end swiper-wrapper -->
      <div class="slide-progress"> <span>01</span>
        <div class="swiper-pagination"></div>
        <span>03</span> </div>
      <!-- end slide-progress -->
      <div class="swiper-button-prev">PREV</div>
      <!-- end button-prev -->
      <div class="swiper-button-next">NEXT</div>
      {{-- <!-- end buttin-next --> --}}
    </div>
    <!-- end gallery-top -->
    <div class="swiper-container gallery-thumbs ">
      <div class="swiper-wrapper">
        <div class="swiper-slide"><span>CCTV</span> <a href="{{route('cctv-show')}}">Road and Traffic Manajement Center</a> </div>
        <div class="swiper-slide"><span>PANTAUAN</span> <a href="#">Arus Lalu Lintas dan Cuaca Lokasi</a></div>
        <div class="swiper-slide"><span>TRAFFIC</span> <a href="#">Kepadatan Lalu Lintas</a></div>
      </div>
      <!-- end swiper-wrapper --> 
    </div>
    <!-- end gallery-thumbs --> 
  </header>