
<!-- FAVICON FILES -->
<link href="{{asset('asset/front-panel/ico/hub.png" rel="apple-touch-icon" sizes="144x144')}}">
<link href="{{asset('asset/front-panel/ico/hub.png" rel="apple-touch-icon" sizes="114x114')}}">
<link href="{{asset('asset/front-panel/ico/hub.png" rel="apple-touch-icon" sizes="72x72')}}">
<link href="{{asset('asset/front-panel/ico/hub.png" rel="apple-touch-icon')}}">
<link href="{{asset('asset/front-panel/ico/hub.png" rel="shortcut icon')}}">

<!-- CSS FILES -->
<link rel="stylesheet" href="{{asset('asset/front-panel/css/fontawesome.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/front-panel/css/fancybox.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/front-panel/css/hamburger.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/front-panel/css/odometer.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/front-panel/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/front-panel/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/front-panel/css/style.css')}}">
