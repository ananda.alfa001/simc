<div id="terminal-bekasi">
    <a href="">
        <img src="{{ url('/storage/' . $link) }}" style="display: none;">
        <img src="{{ url('/storage/' . $save) }}" style="display: none;">
        <img class="img-responsive" src="{{ url('/storage/' . $onair) }}">
    </a>
    <button class="btn btn-warning btcc" type="button" disabled>
        <span class="spinner-grow spinner-grow-sm text-success" role="status" aria-hidden="true"></span>
       TERMINAL BEKASI
    </button>
</div>
