<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>Master Control Room - CCTV RTTMC</title>
    <meta charset="utf-8">

    <link rel="stylesheet" href="{{ asset('asset/front-panel/cctv/css/style-cctv.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/lightgallery/1.3.9/css/lightgallery.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>


    <style>
        body {
            background-color: #030530
        }

    </style>
</head>

<body class="home">
    <div class="container" style="margin-top:40px;">
        <nav class="navbar navbar-expand-sm justify-content-center">
        <ul>    
            <li>
                <h1>RTTMC KEMENHUB</h1>
            </li>
            <li>
                <h2>Road Transport and Traffic Management Center</h2>
            </li>
        </ul>
        <br/>  
        </nav>
            <br>
        <div class="demo-gallery">
            <ul id="lightgallery" class="list-unstyled row">
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.109:21075/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.109:21075/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.109:21075/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                        Simpang Dawuhan 1
                    </button>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.109:21076/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.109:21076/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.109:21076/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                            Simpang Dawuhan 2
                    </button>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.109:21077/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.109:21077/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.109:21077/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                            Simpang Dawuhan 3
                    </button>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.109:21077/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.46:21025/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.46:21025/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                            Tanjungpura 1
                    </button>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.46:21026/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.46:21026/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.46:21026/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                            Tanjungpura 2
                    </button>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.46:21027/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.46:21027/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.46:21027/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                            Tanjungpura 3
                    </button>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2" data-responsive="http://36.93.45.68:21087/video1s3.mjpg?user=admin&pw=Admin123" data-src="http://36.93.45.68:21087/video1s3.mjpg?user=admin&pw=Admin123" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                    <a href="">
                        <img class="img-responsive" src="http://36.93.45.68:21087/video1s3.mjpg?user=admin&pw=Admin123">
                    </a>
                    <button class="btn btn-warning btcc" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm text-success" role="status"
                            aria-hidden="true"></span>
                            Simpang Jatibarang 1
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#lightgallery').lightGallery();
        });
    </script>
    <script src="{{ asset('asset/front-panel/cctv/js/main-cctv.js') }}"></script>
    <script src="{{ asset('asset/front-panel/cctv/js/load/terminalbekasi.js') }}"></script>
</body>

</html>
